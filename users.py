import json
import logging
from contextlib import suppress
from tkinter.simpledialog import askstring

import aiofiles

from exceptions import MissingUserCredentials, TokenException, RegistrationFailed
from utils import open_connection
from write_chat import read_line, write_line

USER_CRED_FILE_NAME = "user_creds"


async def get_user_creds():
    with suppress(FileNotFoundError, json.JSONDecodeError):
        async with aiofiles.open(USER_CRED_FILE_NAME, mode="r") as file:
            json_line = await file.readline()
            line = json.loads(json_line)

            if line:
                nickname = line["nickname"]
                account_hash = line["account_hash"]

                return nickname, account_hash

    raise MissingUserCredentials()


PREFERRED_NICKNAME = ""


def get_preferred_nickname():
    global PREFERRED_NICKNAME

    if not PREFERRED_NICKNAME:
        name = askstring('Новый пользователь', 'Введите ваш новый логин:')
        logging.warning(msg=f"Пользователь ввел новый логин: {name}")
        PREFERRED_NICKNAME = name
        return name

    return PREFERRED_NICKNAME


async def register(host, port):
    async with open_connection(host, port) as (reader, writer):

        # Hello %username%! Enter your personal hash or leave it empty to create new account.
        await read_line(reader)
        await write_line(writer, "")
        await write_line(writer, "\n")

        # Enter preferred nickname below:
        preferred_nickname = get_preferred_nickname()

        await read_line(reader)
        await write_line(writer, preferred_nickname)
        await write_line(writer, "\n")

        # {"nickname": "Vibrant ", "account_hash": "*****"}
        auth_response = await read_line(reader)

        async with aiofiles.open(USER_CRED_FILE_NAME, mode="w") as file:
            await file.write(str(auth_response))


async def prepare_user_creds(host, port, account_hash, nickname):
    if not (account_hash and nickname):
        try:
            nickname, account_hash = await get_user_creds()
        except MissingUserCredentials:
            await register(host, port)

            try:
                nickname, account_hash = await get_user_creds()
            except MissingUserCredentials:
                raise RegistrationFailed()

    return nickname, account_hash


async def authorise(reader, writer, nickname, account_hash):
    logging.info(f"Пытаюсь авторизоваться в качестве пользователя {nickname}")

    await read_line(reader)
    await write_line(writer, account_hash)
    await write_line(writer, "\n")

    auth_response = await read_line(reader)
    auth_response_data = json.loads(auth_response)

    if not auth_response_data:
        raise TokenException()
