import argparse
import asyncio
import logging

from utils import open_connection

DEFAULT_HOST = "minechat.dvmn.org"
DEFAULT_PORT = 5050


async def read_line(reader):
    line = await reader.readuntil()
    line = line.decode()
    logging.debug("reader: %s", line)
    return line


def sanitize_write_line(line):
    line = line.replace("\n", " ")
    line = line.strip()
    line = f"{line}\n"
    return line


async def write_line(writer, line):
    line = sanitize_write_line(line)
    writer.write(line.encode())
    await writer.drain()
    logging.debug("sender: %s", line)


async def submit_message(reader, writer, message="hello world \n"):
    await read_line(reader)
    await write_line(writer, f"{message} \n")
    await write_line(writer, "\n")


async def write_chat(host, port, account_hash, nickname, message):
    from users import prepare_user_creds, authorise

    nickname, account_hash = await prepare_user_creds(host, port, account_hash, nickname)

    async with open_connection(host, port) as (reader, writer):
        await authorise(reader, writer, nickname, account_hash)
        await submit_message(reader, writer, message)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Чат для Зины')
    parser.add_argument("--host", dest="host", const=None, default=DEFAULT_HOST)
    parser.add_argument("--port", dest="port", const=None, default=DEFAULT_PORT)
    parser.add_argument("--token", dest="token", const=None, default=None)
    parser.add_argument("--nickname", dest="nickname", const=None, default=None)
    parser.add_argument("message", type=str)
    args = parser.parse_args()

    logging.basicConfig(level=logging.DEBUG)

    asyncio.run(
        write_chat(
            host=args.host,
            port=args.port,
            account_hash=args.token,
            nickname=args.nickname,
            message=args.message,
        )
    )
