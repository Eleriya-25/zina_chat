import argparse
import asyncio
import aiofiles

from utils import open_connection, format_message

DEFAULT_HOST = "minechat.dvmn.org"
DEFAULT_PORT = 5000
DEFAULT_FILE_PATH = "chat_messages"


async def print_message(file, string):
    message = format_message(string)
    await file.write(message)
    print(message)


async def read_chat(host, port, file_path):
    async with open_connection(host, port) as (reader, writer):

        async with aiofiles.open(file_path, mode="a") as file:
            await print_message(file, "Установлено соединение")

            while True:
                line = await reader.readline()
                await print_message(file, line.decode())


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Чат для Зины')
    parser.add_argument("--host", dest="host", const=None, default=DEFAULT_HOST)
    parser.add_argument("--port", dest="port", const=None, default=DEFAULT_PORT)
    parser.add_argument("--history", dest="file_path", const=None, default=DEFAULT_FILE_PATH)
    args = parser.parse_args()

    asyncio.run(read_chat(args.host, args.port, args.file_path))
