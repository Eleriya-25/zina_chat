class TokenException(Exception):
    message = "Неизвестный токен. Проверьте его или зарегистрируйте заново."


class MissingUserCredentials(Exception):
    message = "Данные пользователя не найдены"


class RegistrationFailed(Exception):
    message = "Ошибка регистрации"
