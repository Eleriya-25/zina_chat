import asyncio
from contextlib import asynccontextmanager
from datetime import datetime


@asynccontextmanager
async def open_connection(host, port):
    reader, writer = None, None

    try:
        reader, writer = await asyncio.open_connection(host, port)
        yield reader, writer

    finally:
        if writer:
            writer.close()
            await writer.wait_closed()


def format_message(message):
    timestamp = datetime.now().strftime("%d.%m.%Y %H:%M")
    end_of_line = '\n' if not message.endswith('\n') else ''
    return f"[{timestamp}] {message}{end_of_line}"
