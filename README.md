# ZINA CHAT

Проект содержит:
- утилиты для наблюдения (read_chat) и участия (write_chat) в чате для задания на платформе DEVMAN
- десктоп интерфейс для работы с чатом (interface)


## Перед запуском

Установка poetry - `pip3 install poetry`. Poetry - менеджер зависимостей. 
Все зависимости перечислены в файле pyproject.toml.


## Запуск сохранения сообщений из чата

```shell
poetry run python ./read_chat.py  --host <хост сервера чата> --port <порт> --history <файл для сохранения потока чата>
```

## Запуск отправления сообщения в чат

```shell
poetry run python ./write_chat.py  --host <хост сервера чата> --port <порт> --token <токен доступа> --nickname <имя пользователя> <сообщение>
```

## Запуск десктоп-интерфейса

```shell
poetry run python ./write_chat.py  --host <хост сервера чата> --read_port <порт для чтения сообщений> --write_port <порт для отправки сообщений> --token <токен доступа> --nickname <имя пользователя> <сообщение>
```
