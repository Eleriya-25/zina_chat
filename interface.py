import argparse
import asyncio
import logging
from contextlib import suppress
from tkinter import messagebox

import aiofiles
from anyio import create_task_group
from async_timeout import timeout

import gui
from exceptions import TokenException
from utils import open_connection, format_message
from read_chat import print_message
from write_chat import submit_message
from users import prepare_user_creds, authorise

DEFAULT_HOST = "minechat.dvmn.org"
DEFAULT_READ_PORT = 5000
DEFAULT_WRITE_PORT = 5050
DEFAULT_FILE_PATH = "chat_messages"

CONNECTION_TIMEOUT = 1


async def read_messages(host, port, queues):
    def put_message_in_queues(message):
        for queue in queues:
            queue.put_nowait(message)

    status_updates_queue.put_nowait(gui.ReadConnectionStateChanged.INITIATED)

    async with open_connection(host, port) as (reader, writer):
        put_message_in_queues("Установлено соединение")
        status_updates_queue.put_nowait(gui.ReadConnectionStateChanged.ESTABLISHED)

        try:
            while True:
                line = await reader.readline()
                put_message_in_queues(line.decode())
                watchdog_queue.put_nowait("New message in chat")
        except ConnectionError:
            status_updates_queue.put_nowait(gui.ReadConnectionStateChanged.CLOSED)


async def save_messages(filepath, queue):
    async with aiofiles.open(filepath, mode="a") as file:

        while True:
            line = await queue.get()
            await print_message(file, line)


async def load_history(filepath, queue):
    async with aiofiles.open(filepath, mode="r") as file:
        queue.put_nowait("Предыдущие сообщения:")

        async for line in file:
            queue.put_nowait(line)


async def do_authorization(reader, writer, nickname, account_hash):
    watchdog_queue.put_nowait("Prompt before auth")

    try:
        await authorise(reader, writer, nickname, account_hash)

        event = gui.NicknameReceived(nickname)
        status_updates_queue.put_nowait(event)
    except TokenException:
        messagebox.showinfo("Error", "Проверьте токен, сервер его не узнал")
        return

    watchdog_queue.put_nowait("Authorization done")


async def send_messages(host, port, queue, token, nickname):
    nickname, account_hash = await prepare_user_creds(host, port, token, nickname)

    status_updates_queue.put_nowait(gui.SendingConnectionStateChanged.INITIATED)

    async with open_connection(host, port) as (reader, writer):
        status_updates_queue.put_nowait(gui.SendingConnectionStateChanged.ESTABLISHED)

        try:
            await do_authorization(reader, writer, nickname, account_hash)

            while True:
                message = await queue.get()
                await submit_message(reader, writer, message)
                watchdog_queue.put_nowait("Message sent")

        except ConnectionError:
            status_updates_queue.put_nowait(gui.SendingConnectionStateChanged.CLOSED)


async def watch_for_connection(queue):
    watchdog_logger = logging.getLogger("watchdog_logger")
    watchdog_logger.setLevel(logging.INFO)

    while True:
        try:
            async with timeout(CONNECTION_TIMEOUT) as cm:
                while True:
                    message = await queue.get()
                    watchdog_logger.info(msg=format_message(f"Connection_is_alive. {message}"))
                    cm.shift(CONNECTION_TIMEOUT)

        except asyncio.exceptions.TimeoutError:
            try:
                sending_queue.put_nowait("")
                watchdog_queue.put_nowait("Ping message sent")
            except ConnectionError:
                watchdog_logger.info(msg=format_message("1s timeout is elapsed"))
                raise ConnectionError()


async def handle_connection(host, read_port, write_port, token, nickname):
    while True:
        try:
            async with create_task_group() as tg:
                tg.start_soon(load_history, DEFAULT_FILE_PATH, messages_queue)
                tg.start_soon(read_messages, host, read_port, [messages_queue, file_queue])
                tg.start_soon(save_messages, DEFAULT_FILE_PATH, file_queue)
                tg.start_soon(send_messages, host, write_port, sending_queue, token, nickname)
                tg.start_soon(watch_for_connection, watchdog_queue)
        except ConnectionError:
            logging.error("Connection closed. Reconnect...")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Чат для Зины')
    parser.add_argument("--host", dest="host", const=None, default=DEFAULT_HOST)
    parser.add_argument("--read_port", dest="read_port", const=None, default=DEFAULT_READ_PORT)
    parser.add_argument("--write_port", dest="write_port", const=None, default=DEFAULT_WRITE_PORT)
    parser.add_argument("--token", dest="token", const=None, default=None)
    parser.add_argument("--nickname", dest="nickname", const=None, default=None)
    args = parser.parse_args()

    messages_queue = asyncio.Queue()
    sending_queue = asyncio.Queue()
    status_updates_queue = asyncio.Queue()
    file_queue = asyncio.Queue()
    watchdog_queue = asyncio.Queue()

    with suppress(gui.TkAppClosed):
        loop = asyncio.get_event_loop()
        loop.create_task(handle_connection(args.host, args.read_port, args.write_port, args.token, args.nickname))
        loop.run_until_complete(gui.draw(messages_queue, sending_queue, status_updates_queue))
